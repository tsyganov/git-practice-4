type Message = {
  author: string;
  message: string;
}

console.log('Hello, world!');

const trySomething = (message: string) => {
  console.log(message);
};

trySomething('123');